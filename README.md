#Osu Music Library

Osu Music Library is a tool to organize and look up your osu! songs from outside the game and was created to make it easier to plan songs ahead of time in a multiplayer lobby. You can store songs in categories, similar to the collections in osu!, but you can also rate them and add them to a queue. There are also lots of filtering options and ways to sort stuff to make it easier to find what you're looking for.

The program regularly scans your osu! song folder, but doesn't interact with the game itself or change any of the game's files.

[![Screenshot](http://i.imgur.com/aq09xL0.png)](http://i.imgur.com/9bSMp0x.png)

*(Click for a larger version)*

##Download
Version 1.2 is available for download from the [downloads page](https://bitbucket.org/Dayanto/osu-music-library/downloads).

##Requirements
This project uses .Net Framework 4.5, so if you don't have that, you will have to download it first.

##Getting Started
After downloading, just unzip the application anywhere, together with the .dll file, and run it. The first time, it will ask you to locate your osu! installation path. It will automatically try to detect the installation if it's in the usual path. Otherwise you will have to locate it manually.

There are a few things to note

* By default, the program regularly scans you songs folder while it's open. (once a minute) If you have a slow computer, it's probably a good idea to disable this in the settings, so that you don't get any random lag spikes in the middle of a song. I will then only run a scan once every time you open the program.
* If you put the application in a restricted location such as Program Files, you will have to grant it administrator rights or it won't have write access. Without write access, the application will freeze. 
* Try to avoid running multiple instances of the program at once, because they will compete over the save files and in worst case, you might lose data. (your categories and ratings) 

##How to use the program

Detailed information about all the features is available in the wiki, [here](https://bitbucket.org/Dayanto/osu-music-library/wiki/How%20to%20use%20the%20program).

##License / Credits
The project itself is in the public domain, so you are free to use the code for any purposes however you see fit. However a couple of images that I use are under different licenses. Information about that stuff can be found [here](https://bitbucket.org/Dayanto/osu-music-library/src/67bb88f46b3d767cc0541bdfd7c4a996f4073b8e/license/?at=master).

## More Screenshots

[![Screenshot](http://i.imgur.com/YzGARam.png)](http://i.imgur.com/2aDfayC.png) 

*Demonstrates filtering by difficulty and rating as well as using the queue*

[![Screenshot](http://i.imgur.com/PPHCrVJ.png)](http://i.imgur.com/zgbmQK0.png)

*Demonstrates categories and sorting by rating*