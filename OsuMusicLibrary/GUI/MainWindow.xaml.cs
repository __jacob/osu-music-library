﻿using OsuMusicLibrary.Data;
using OsuMusicLibrary.Data.Containers;
using OsuMusicLibrary.Media;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using OsuMusicLibrary.Data.Types;
using Wpf.Util;
using System.Diagnostics;
using System.ComponentModel;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, INotifyPropertyChanged
	{
		public static MainWindow OMLWindow = null;

		private const int sliderMax = 100;
		private bool sliderDragged = false;

		#region Window Construction

		public MainWindow()
		{
			// Initialize the xaml stuff
			InitializeComponent();

			// Attach the list of songs to the listview, it will filter itself based on the ListViewFilter function below
			CollectionViewSource listViewSource = this.Resources["ListViewSource"] as CollectionViewSource;
			listViewSource.Source = GlobalData.ObservableSongList;

			// Attach the list of songs to the listview, it will use the queue as a filter based on QueueFilter below
			CollectionViewSource queueSource = this.Resources["QueueSource"] as CollectionViewSource;
			queueSource.Source = GlobalData.Session.Queue;

			// Add the items to the category list
			List<object> categoryItems = new List<object>();
			categoryItems.Add(new AllSongs());
			categoryItems.Add(new CategoryGroup());
			CategoryView.ItemsSource = categoryItems;

			// Set up the context menu for the tree view
			ContextCategories.ItemsSource = GlobalData.Library.Categories;

			// Set the number of notches in the slider
			Slider.Maximum = sliderMax;

            // Initialize the filters and sort directives
            RestoreFilteringAndSorting(GlobalData.Session.CurrentFilter, GlobalData.Session.CurrentSortDirective);

            this.PreviewKeyDown += OnKeyDownHandler;
            this.PreviewKeyUp += OnKeyUpHandler;

			// Make the window openly accessible
			OMLWindow = this;
		}

		private void ListViewFilter(object sender, FilterEventArgs e)
		{
			SongData song = e.Item as SongData;
            e.Accepted = GlobalData.Session.CurrentFilter.IsVisible(song);
		}

		private void QueueFilter(object sender, FilterEventArgs e)
		{
			SongData song = e.Item as SongData;
			e.Accepted = GlobalData.Session.Queue.Contains(song);
		}

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (!SearchBox.IsFocused)
            {
                SearchBox.Focus();
            }
        }

        private void OnKeyUpHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                SearchBox.Text = "";
                GlobalData.Session.CurrentFilter.SearchText = "";
                GlobalData.Session.CurrentFilter.SearchTerms = new String[0];
            }
        }

		#endregion

        #region Column Width Properties

        #region INotify
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private GridLength rightColumnWidth = new GridLength(0, GridUnitType.Auto);
        public GridLength RightColumnWidth
        {
            get
            {
                Console.WriteLine("!");
                // If not yet set, get the starting value from the DataModel
                if (rightColumnWidth.IsAuto)
                    rightColumnWidth = new GridLength(270, GridUnitType.Pixel);
                return rightColumnWidth;
            }
            set
            {
                Console.WriteLine("!");
                rightColumnWidth = value;
                OnPropertyChanged("RightColumnWidth");
            }
        }

        #endregion

        #region Common Functions

        public void SetSliderPosition(double position)
		{
			if(!sliderDragged) Slider.Value = position * sliderMax;
		}

		public void PlaySong(SongContext context, SongData song)
		{
			if (song == null) return;
			if (!File.Exists(song.AudioFilePath)) return;
			MusicPlayer.SetCurrentSong(context, song);
			MusicPlayer.Play();
		}

		private void CreateCategory(String name)
		{
			// Add the category
			GlobalData.Library.CreateCategory(name);
		}

		private void SaveQueue(String name)
		{
			// Add the category
			GlobalData.Library.CreateCategoryFromQueue(name);
		}

		private bool VerifyCategoryName(String name)
		{
			// Check if the name is valid
			if (name != null && name.Replace(" ", "").Replace("\t", "") != String.Empty)
			{
				// Check if the category exists
				if (!GlobalData.Library.CategoryExists(name))
				{
					return true;
				}
				else
				{
					DialogBox.Display("Category Exists", "A category with that name already exists!");
				}
			}
			else
			{
				DialogBox.Display("Invalid Name", "You need to give your category a name");
			}
			return false;
		}

		private void AddCategoryToQueue()
		{
			Category category = CategoryView.SelectedItem as Category;

			if (category != null)
			{
				foreach (int songID in category.SongIDs)
				{
					SongData song;
					if (GlobalData.SongCache.Songs.TryGetValue(songID, out song))
					{
						GlobalData.Session.Queue.Add(song);
					}
				}
				CollectionViewSource.GetDefaultView(QueueList.ItemsSource).Refresh();
			}
		}

        private void RestoreFilteringAndSorting(Filter filter, SortDirective sort)
        {
            // Restore previous sorting
            GridViewSort.UpdateColumnSort(sort.PropertyName, sort.IsAscending);

            // Set the checkboxes to their respective states
            Easy.IsChecked = filter.ShowEasySongs;
            Normal.IsChecked = filter.ShowNormalSongs;
            Hard.IsChecked = filter.ShowHardSongs;
            Insane.IsChecked = filter.ShowInsaneSongs;
            ShowUnrated.IsChecked = filter.ShowUnrated;

            // Set the text in te text box
            SearchBox.Text = filter.SearchText;

            // Set the state of the different combo boxes
            
            // Game modes
            if (filter.SelectedGameMode == null) ComboBox_Modes.SelectedItem = AnyMode;
            else if (filter.SelectedGameMode == GameMode.OSU) ComboBox_Modes.SelectedItem = Osu;
            else if (filter.SelectedGameMode == GameMode.TAIKO) ComboBox_Modes.SelectedItem = Taiko;
            else if (filter.SelectedGameMode == GameMode.CATCH_THE_BEAT) ComboBox_Modes.SelectedItem = CatchTheBeat;
            else if (filter.SelectedGameMode == GameMode.OSU_MANIA) ComboBox_Modes.SelectedItem = OsuMania;
            
            // Minimum Rating
            if (filter.MinimumRating == 1) ComboBox_Ratings.SelectedItem = MinRating1;
            else if (filter.MinimumRating == 2) ComboBox_Ratings.SelectedItem = MinRating2;
            else if (filter.MinimumRating == 3) ComboBox_Ratings.SelectedItem = MinRating3;
            else if (filter.MinimumRating == 4) ComboBox_Ratings.SelectedItem = MinRating4;
            else if (filter.MinimumRating == 5) ComboBox_Ratings.SelectedItem = MinRating5;
            else if (filter.MinimumRating == 6) ComboBox_Ratings.SelectedItem = MinRating6;
            
            // Downloaded
            if (filter.TimeSinceRequired == Downloaded.ANY_TIME) ComboBox_Downloaded.SelectedItem = AnyTime;
            else if (filter.TimeSinceRequired == Downloaded.TODAY) ComboBox_Downloaded.SelectedItem = Today;
            else if (filter.TimeSinceRequired == Downloaded.LAST_3_DAYS) ComboBox_Downloaded.SelectedItem = Last3Days;
            else if (filter.TimeSinceRequired == Downloaded.LAST_WEEK) ComboBox_Downloaded.SelectedItem = LastWeek;
            else if (filter.TimeSinceRequired == Downloaded.LAST_MONTH) ComboBox_Downloaded.SelectedItem = LastMonth;
        }

		#endregion

		/*########################## Event activated methods ##################################*/

		#region Menu Bar

		private void OpenSettings_Click(object sender, RoutedEventArgs e)
		{
			SettingsWindow settingsWindow = new SettingsWindow();
			settingsWindow.Show();
		}

		private void Exit_Click(object sender, RoutedEventArgs e)
		{
			App.Close();
		}

		private void RebuildCache_Click(object sender, RoutedEventArgs e)
		{
			GlobalData.CreateNewSongCache();
			GlobalData.SongCache.Synchronize();
		}

		private void CreateCategory_Click(object sender, RoutedEventArgs e)
		{
			InputDialog.Display("Create a new category", "Category Name", "New Category", "Create", VerifyCategoryName, CreateCategory);
		}

		private void SaveQueue_Click(object sender, RoutedEventArgs e)
		{
			InputDialog.Display("Save queue as category", "Category Name", "New Category", "Save", VerifyCategoryName, SaveQueue);
		}

		#endregion

		#region Music Player

		/**
		 * Responds to the play / pause button being pressed
		 */
		private void ButtonClickPlayPause(object sender, RoutedEventArgs e)
		{
			if (MusicPlayer.Player.Source == null) return;

			if (PlayPause.Content == FindResource("Play"))
			{
				if (MusicPlayer.Player.NaturalDuration != MusicPlayer.Player.Position)
				{
					MusicPlayer.Play();
				}
			}
			else
			{
				MusicPlayer.Pause();
			}
		}

		/**
		 * Responds to the next button being pressed
		 */
		private void ButtonClickNext(object sender, RoutedEventArgs e)
		{
			MusicPlayer.PlayNextSong();
		}

		/**
		 * Responds to the next button being pressed
		 */
		private void ButtonClickPrevious(object sender, RoutedEventArgs e)
		{
			MusicPlayer.PlayPreviousSong();
		}

		private void SliderPressed(object sender, MouseButtonEventArgs e)
		{
			sliderDragged = true;
		}

		private void SliderReleased(object sender, MouseButtonEventArgs e)
		{
			MusicPlayer.SetTime(Slider.Value / sliderMax);
			sliderDragged = false;
		}

		#endregion

		#region ListView

        #region Sort

        private void SortByColumn(Func<SongData, object> sortProperty, String propertyName)
        {
            SortDirective sort = GlobalData.Session.CurrentSortDirective;

            if (sort.PropertyName != propertyName) sort.IsAscending = true;
            else sort.IsAscending = !sort.IsAscending;

            sort.SortProperty = sortProperty;
            sort.PropertyName = propertyName;

            GridViewSort.UpdateColumnSort(sort.PropertyName, sort.IsAscending);
        }

        private void SortByTitle(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.DisplayTitle, "DisplayTitle");
        }
        private void SortByArtist(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.DisplayArtist, "DisplayArtist");
        }
        private void SortByTime(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.Duration.TotalMilliSeconds, "Duration.TotalMilliSeconds");
        }
        private void SortBySource(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.DisplaySource, "DisplaySource");
        }
        private void SortByRating(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.Rating, "Rating");
        }
        private void SortByDownloaded(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.DateDownloaded, "DateDownloaded");
        }
        private void SortByCreator(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.Creator, "Creator");
        }

        #endregion

        #region List

        protected void ListViewDoubleClicked(object sender, MouseButtonEventArgs e)
		{
			SongData song = ((ListViewItem)sender).Content as SongData;
            SongContext context = GlobalData.Session.CurrentCategory == null ? SongContext.ALL_SONGS : SongContext.CATEGORY;
			PlaySong(context, song);
			PlayPause.Content = FindResource("Pause");
		}

        // Rate songs via the stars

        private void ButtonClick_Rate1Star(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            SongData song = button.DataContext as SongData;
            GlobalData.Library.RateSong(song, 1);
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }
        private void ButtonClick_Rate2Star(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            SongData song = button.DataContext as SongData;
            GlobalData.Library.RateSong(song, 2);
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }
        private void ButtonClick_Rate3Star(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            SongData song = button.DataContext as SongData;
            GlobalData.Library.RateSong(song, 3);
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }
        private void ButtonClick_Rate4Star(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            SongData song = button.DataContext as SongData;
            GlobalData.Library.RateSong(song, 4);
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }
        private void ButtonClick_Rate5Star(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            SongData song = button.DataContext as SongData;
            GlobalData.Library.RateSong(song, 5);
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }

        #endregion

        #region Context Menu


		/**
		 * Play songs from the context menu
		 */
		private void ContextMenu_Play(object sender, RoutedEventArgs e)
		{
            SongContext context = GlobalData.Session.CurrentCategory == null ? SongContext.ALL_SONGS : SongContext.CATEGORY;
			PlaySong(context, (SongData)SongList.SelectedItem);
		}

		private void ContextMenu_MoreInfo(object sender, RoutedEventArgs e)
		{
			SongData song = (SongData)SongList.SelectedItem;
			if (song != null)
			{
				SongInfo window = new SongInfo(song);
				window.Show();
			}
		}

        private void ContextMenu_ShowFolder(object sender, RoutedEventArgs e)
        {
            SongData song = (SongData)SongList.SelectedItem;
            if (song != null)
            {
                Process.Start(song.DirectoryPath);
            }
        }

        private void ContextMenu_AddToCategory(object sender, RoutedEventArgs e)
        {
            if (SongList.SelectedItems.Count == 0) return;

            MenuItem menuItem = sender as MenuItem;
            Category category = GlobalData.Library.GetCategory((String)menuItem.Header);

            // Ask the user for a confirmation
            if (SongList.SelectedItems.Count > 2 && GlobalData.Settings.ConfirmMultiActions)
            {
                MessageBoxResult result = MessageBox.Show("You're about to add " + SongList.SelectedItems.Count + " songs to \"" + category.Name + "\". \nAre you sure? \n\n(Uncheck Settings->\"Confirm actions on multiple files\" to disable these messages.)", "Add to category", MessageBoxButton.OKCancel, MessageBoxImage.Information);
                if (result != MessageBoxResult.OK) return;
            }

            foreach (SongData song in SongList.SelectedItems)
            {
                category.AddSong(song);
            }

            GlobalData.SaveLibrary();
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }


        private void ContextMenu_AddToQueue(object sender, RoutedEventArgs e)
        {
            if (SongList.SelectedItems.Count == 0) return;

            // Ask the user for a confirmation
            if (SongList.SelectedItems.Count > 2 && GlobalData.Settings.ConfirmMultiActions)
            {
                MessageBoxResult result = MessageBox.Show("You're about to add " + SongList.SelectedItems.Count + " songs to the queue.\nAre you sure? \n\n(Uncheck Settings->\"Confirm actions on multiple files\" to disable these messages.)", "Add to queue", MessageBoxButton.OKCancel, MessageBoxImage.Information);
                if (result != MessageBoxResult.OK) return;
            }

            foreach (SongData song in SongList.SelectedItems)
            {
                if (!GlobalData.Session.Queue.Contains(song))
                {
                    GlobalData.Session.Queue.Add(song);
                }
            }

            CollectionViewSource.GetDefaultView(QueueList.ItemsSource).Refresh();
        }

        private void ContextMenu_RemoveFromCategory(object sender, RoutedEventArgs e)
        {
            Category category = GlobalData.Session.CurrentCategory;
            if (SongList.SelectedItems.Count == 0) return;

            if (category == null)
            {
                DialogBox.Display("Invalid Operation", "Cannot remove songs from 'All Songs'");
                return;
            }

            // Ask the user for a confirmation
            if (SongList.SelectedItems.Count > 2 && GlobalData.Settings.ConfirmMultiActions)
            {
                MessageBoxResult result = MessageBox.Show("You're about to remove " + SongList.SelectedItems.Count + " songs from \"" + category.Name + "\". \nAre you sure? \n\n(Uncheck Settings->\"Confirm actions on multiple files\" to disable these messages.)", "Remove from category", MessageBoxButton.OKCancel, MessageBoxImage.Information);
                if (result != MessageBoxResult.OK) return;
            }

            foreach (SongData song in SongList.SelectedItems)
            {
                category.SongIDs.Remove(song.SongID);
            }

            GlobalData.SaveLibrary();
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }


		//  Rate songs via the context menu (1-5)

        private void RateSongsFromContextMenu(int rating)
        {
            if (SongList.SelectedItems.Count == 0) return;

            // Ask the user for a confirmation
            if (SongList.SelectedItems.Count > 2 && GlobalData.Settings.ConfirmMultiActions)
            {
                MessageBoxResult result = MessageBox.Show("You're about to give " + SongList.SelectedItems.Count + " songs a " + rating + " star rating.\nAre you sure? \n\n(Uncheck Settings->\"Confirm actions on multiple files\" to disable these messages.)", "Rate song", MessageBoxButton.OKCancel, MessageBoxImage.Information);
                if (result != MessageBoxResult.OK) return;
            }

            // Give each selected song a rating.
            foreach (SongData song in SongList.SelectedItems.OfType<SongData>())
            {
                GlobalData.Library.RateSong(song, rating);
            }
            
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }

		private void ContextMenu_Rate1(object sender, RoutedEventArgs e)
		{
            RateSongsFromContextMenu(1);
		}
		private void ContextMenu_Rate2(object sender, RoutedEventArgs e)
		{
            RateSongsFromContextMenu(2);
		}
		private void ContextMenu_Rate3(object sender, RoutedEventArgs e)
		{
            RateSongsFromContextMenu(3);
		}
		private void ContextMenu_Rate4(object sender, RoutedEventArgs e)
		{
            RateSongsFromContextMenu(4);
		}
		private void ContextMenu_Rate5(object sender, RoutedEventArgs e)
		{
            RateSongsFromContextMenu(5);
		}

		/**
		 * Remove the rating from a song via the Source menu
		 */
		private void ContextMenu_RemoveRating(object sender, RoutedEventArgs e)
		{
            if (SongList.SelectedItems.Count == 0) return;

            // Ask the user for a confirmation
            if (SongList.SelectedItems.Count > 2 && GlobalData.Settings.ConfirmMultiActions)
            {
                MessageBoxResult result = MessageBox.Show("You're about to remove the rating from " + SongList.SelectedItems.Count + " songs.\nAre you sure? \n\n(Uncheck Settings->\"Confirm actions on multiple files\" to disable these messages.)", "Remove rating", MessageBoxButton.OKCancel, MessageBoxImage.Information);
                if (result != MessageBoxResult.OK) return;
            }

            // Give each selected song a rating.
            foreach (SongData song in SongList.SelectedItems.OfType<SongData>())
            {
                GlobalData.Library.RemoveRating(song);
            }

            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

        #endregion

        #endregion

        #region Queue

        protected void QueueDoubleClicked(object sender, MouseButtonEventArgs e)
		{
			SongData song = ((ListBoxItem)sender).Content as SongData;
			PlaySong(SongContext.QUEUE, song);
			PlayPause.Content = FindResource("Pause");
		}

		/**
		 * Play songs from the queue's context menu
		 */
		private void QueueContextMenu_Play(object sender, RoutedEventArgs e)
		{
			PlaySong(SongContext.QUEUE, (SongData)QueueList.SelectedItem);
		}

		private void QueueContextMenu_MoreInfo(object sender, RoutedEventArgs e)
		{
			SongData song = (SongData)QueueList.SelectedItem;
			if (song != null)
			{
				SongInfo window = new SongInfo(song);
				window.Show();
			}
		}
		
		private void QueueContextMenu_MoveDown(object sender, RoutedEventArgs e)
		{
			SongData selectedSong = (SongData)QueueList.SelectedItem;
			if (selectedSong == null) return;
			if (selectedSong != GlobalData.Session.Queue.Last())
			{
				int newIndex = GlobalData.Session.Queue.IndexOf(selectedSong) + 1;
				GlobalData.Session.Queue.Remove(selectedSong);
				GlobalData.Session.Queue.Insert(newIndex, selectedSong);
				CollectionViewSource.GetDefaultView(QueueList.ItemsSource).Refresh();
			}
		}

		private void QueueContextMenu_MoveUp(object sender, RoutedEventArgs e)
		{
			SongData selectedSong = (SongData)QueueList.SelectedItem;
			if (selectedSong == null) return;
			if (selectedSong != GlobalData.Session.Queue.First())
			{
				int newIndex = GlobalData.Session.Queue.IndexOf(selectedSong) - 1;
				GlobalData.Session.Queue.Remove(selectedSong);
				GlobalData.Session.Queue.Insert(newIndex, selectedSong);
				CollectionViewSource.GetDefaultView(QueueList.ItemsSource).Refresh();
			}
		}

		private void QueueContextMenu_Remove(object sender, RoutedEventArgs e)
		{
            if (QueueList.SelectedItems.Count == 0) return;

            // Ask the user for a confirmation
            if (QueueList.SelectedItems.Count > 2 && GlobalData.Settings.ConfirmMultiActions)
            {
                MessageBoxResult result = MessageBox.Show("You're about to remove " + QueueList.SelectedItems.Count + " songs from the queue.\nAre you sure? \n\n(Uncheck Settings->\"Confirm actions on multiple files\" to disable these messages.)", "Remove from queue", MessageBoxButton.OKCancel, MessageBoxImage.Information);
                if (result != MessageBoxResult.OK) return;
            }

            // The selected list needs to be copied to avoid issues when removing elements from the same list
            List<SongData> tempList = new List<SongData>();
            foreach (SongData song in QueueList.SelectedItems) tempList.Add(song);

            // Remove songs from the queue
            foreach (SongData song in tempList)
            {
                if (GlobalData.Session.Queue.Contains(song))
                {
                    GlobalData.Session.Queue.Remove(song);
                    CollectionViewSource.GetDefaultView(QueueList.ItemsSource).Refresh();
                }
            }
		}

		private void QueueContextMenu_Save(object sender, RoutedEventArgs e)
		{
			InputDialog.Display("Save queue as category", "Category Name", "New Category", "Save", VerifyCategoryName, SaveQueue);
		}

		private void QueueContextMenu_Clear(object sender, RoutedEventArgs e)
		{
			GlobalData.Session.Queue.Clear();
			CollectionViewSource.GetDefaultView(QueueList.ItemsSource).Refresh();
		}

		#endregion

		#region Filter Updates

		/**
		 * Show all songs containing the difficulty easy
		 */
		private void CheckBox_Easy(object sender, RoutedEventArgs e)
		{
			CheckBox box = sender as CheckBox;
            GlobalData.Session.CurrentFilter.ShowEasySongs = box.IsChecked.Value;
			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

		/**
		 * Show all songs containing the difficulty normal
		 */
		private void CheckBox_Normal(object sender, RoutedEventArgs e)
		{
			CheckBox box = sender as CheckBox;
            GlobalData.Session.CurrentFilter.ShowNormalSongs = box.IsChecked.Value;
			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

		/**
		 * Show all songs containing the difficulty hard
		 */
		private void CheckBox_Hard(object sender, RoutedEventArgs e)
		{
			CheckBox box = sender as CheckBox;
            GlobalData.Session.CurrentFilter.ShowHardSongs = box.IsChecked.Value;
			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

		/**
		 * Show all songs containing the difficulty insane
		 */
		private void CheckBox_Insane(object sender, RoutedEventArgs e)
		{
			CheckBox box = sender as CheckBox;
            GlobalData.Session.CurrentFilter.ShowInsaneSongs = box.IsChecked.Value;
			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

		/**
		 * Update the currently visible modes based on the new combo box selection
		 */
		private void ComboBox_Modes_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
            if (ComboBox_Modes.SelectedItem == AnyMode)      GlobalData.Session.CurrentFilter.SelectedGameMode = null;
            if (ComboBox_Modes.SelectedItem == Osu)          GlobalData.Session.CurrentFilter.SelectedGameMode = GameMode.OSU;
            if (ComboBox_Modes.SelectedItem == Taiko)        GlobalData.Session.CurrentFilter.SelectedGameMode = GameMode.TAIKO;
            if (ComboBox_Modes.SelectedItem == CatchTheBeat) GlobalData.Session.CurrentFilter.SelectedGameMode = GameMode.CATCH_THE_BEAT;
            if (ComboBox_Modes.SelectedItem == OsuMania)     GlobalData.Session.CurrentFilter.SelectedGameMode = GameMode.OSU_MANIA;

			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

		/**
		 * Update the currently visible ratings based on the new combo box selection
		 */
		private void ComboBox_Ratings_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if      (ComboBox_Ratings.SelectedItem == MinRating1) GlobalData.Session.CurrentFilter.MinimumRating = 1;
			else if (ComboBox_Ratings.SelectedItem == MinRating2) GlobalData.Session.CurrentFilter.MinimumRating = 2;
			else if (ComboBox_Ratings.SelectedItem == MinRating3) GlobalData.Session.CurrentFilter.MinimumRating = 3;
			else if (ComboBox_Ratings.SelectedItem == MinRating4) GlobalData.Session.CurrentFilter.MinimumRating = 4;
			else if (ComboBox_Ratings.SelectedItem == MinRating5) GlobalData.Session.CurrentFilter.MinimumRating = 5;
            else if (ComboBox_Ratings.SelectedItem == MinRating6) GlobalData.Session.CurrentFilter.MinimumRating = 6;

			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

        /**
		 * Update whether unrated songs should be displayed
		 */
        private void CheckBox_ShowUnrated(object sender, RoutedEventArgs e)
        {
            CheckBox box = sender as CheckBox;
            GlobalData.Session.CurrentFilter.ShowUnrated = box.IsChecked.Value;
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }

		/**
		 * Update how recently downloaded songs are shown based on the new combo box selection
		 */
		private void ComboBox_Downloaded_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (ComboBox_Downloaded.SelectedItem == AnyTime) GlobalData.Session.CurrentFilter.TimeSinceRequired = Downloaded.ANY_TIME;
			if (ComboBox_Downloaded.SelectedItem == Today) GlobalData.Session.CurrentFilter.TimeSinceRequired = Downloaded.TODAY;
			if (ComboBox_Downloaded.SelectedItem == Last3Days) GlobalData.Session.CurrentFilter.TimeSinceRequired = Downloaded.LAST_3_DAYS;
			if (ComboBox_Downloaded.SelectedItem == LastWeek) GlobalData.Session.CurrentFilter.TimeSinceRequired = Downloaded.LAST_WEEK;
			if (ComboBox_Downloaded.SelectedItem == LastMonth) GlobalData.Session.CurrentFilter.TimeSinceRequired = Downloaded.LAST_MONTH;

			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

		/**
		 * Updates the search whenever something is typed in the search box
		 */
		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			String input = SearchBox.Text.ToLower();
			List<String> searchTerms = new List<String>();

			// Skip splitting by spaces inside of quotes
			bool insideQuotes = false;
			foreach (String chunk in input.Split('\"'))
			{
				// If the current chunk is inside quotes, add it to the list of search terms in one piece
				if (insideQuotes) searchTerms.Add(chunk);

				// If outside of quotes, split up every word into a separate string
				if (!insideQuotes) searchTerms.AddRange(input.Split(new String[] { " " }, StringSplitOptions.RemoveEmptyEntries));

				// For every quotation mark, flip whether the text following it is inside or outside quotes
				insideQuotes = !insideQuotes;
			}

			// Filter out any potential spaces from the results (only matters for text inside quotes)
			searchTerms.RemoveAll(x => x == "");

            // Store the current input text
            GlobalData.Session.CurrentFilter.SearchText = SearchBox.Text;

			// Update the current search terms and refresh the filtering
			GlobalData.Session.CurrentFilter.SearchTerms = searchTerms.ToArray();
			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

		#endregion

		#region Tree View

		private void CategoryContextMenu_AddToQueue(object sender, RoutedEventArgs e)
		{
			Category category = CategoryView.SelectedItem as Category;
			if (category != null)
			{
				ConfirmationDialog.Display("Add Songs To Queue", "Are you sure that you want to add all the songs from the category \"" + category.Name + "\" to the queue?", AddCategoryToQueue, () => { }, 140);
			}
		}

		private void CategoryContextMenu_MoveDown(object sender, RoutedEventArgs e)
		{
			Category selectedCategory = CategoryView.SelectedItem as Category;
			if (selectedCategory == null) return;
			if (selectedCategory != GlobalData.Library.Categories.Last())
			{
				int newIndex = GlobalData.Library.Categories.IndexOf(selectedCategory) + 1;
				GlobalData.Library.Categories.Remove(selectedCategory);
				GlobalData.Library.Categories.Insert(newIndex, selectedCategory);
			}
		}

		private void CategoryContextMenu_MoveUp(object sender, RoutedEventArgs e)
		{
			Category selectedCategory = CategoryView.SelectedItem as Category;
			if (selectedCategory == null) return;
			if (selectedCategory != GlobalData.Library.Categories.First())
			{
				int newIndex = GlobalData.Library.Categories.IndexOf(selectedCategory) - 1;
				GlobalData.Library.Categories.Remove(selectedCategory);
				GlobalData.Library.Categories.Insert(newIndex, selectedCategory);
			}
		}

		private void CategoryContextMenu_Rename(object sender, RoutedEventArgs e)
		{
			Category category = CategoryView.SelectedItem as Category;
			if (category != null)
			{
				InputDialog.Display("Rename category", "Category Name", category.Name, "Rename", VerifyCategoryName, renameCategory);
			}
		}

		private void CategoryContextMenu_Delete(object sender, RoutedEventArgs e)
		{
			Category category = CategoryView.SelectedItem as Category;
			if (category != null)
			{
				ConfirmationDialog.Display("Delete Category", "Are you sure that you want to remove the category \"" + category.Name + "\"?", deleteCategory, () => { }, 120);
			}
		}

		private void renameCategory(String name)
		{
			Category category = CategoryView.SelectedItem as Category;
			if (category != null)
			{
				GlobalData.Library.RenameCategory(category, name);
				GlobalData.SaveLibrary();
				CollectionViewSource.GetDefaultView(CategoryView.ItemsSource).Refresh();
			}
		}

		private void deleteCategory()
		{
			Category category = CategoryView.SelectedItem as Category;
			if (category != null)
			{
				GlobalData.Library.RemoveCategory(category.Name);
				GlobalData.SaveLibrary();

                if (category == GlobalData.Session.CurrentCategory) GlobalData.Session.ShowAllSongs();
				CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
			}
		}

		private void CategoryViewLeftMouseDown(object sender, MouseButtonEventArgs e)
		{
			TreeViewItem ClickedTreeViewItem = new TreeViewItem();

			// Find the original object that raised the event
			UIElement ClickedItem = VisualTreeHelper.GetParent(e.OriginalSource as UIElement) as UIElement;

			// Find the clicked TreeViewItem
			while ((ClickedItem != null) && !(ClickedItem is TreeViewItem))
			{
				ClickedItem = VisualTreeHelper.GetParent(ClickedItem) as UIElement;
			}

			TreeViewItem treeViewItem = ClickedItem as TreeViewItem;
			Category category = treeViewItem.Header as Category;
			
			if (treeViewItem.Header is CategoryGroup) return;

			if (category != null)
			{
				GlobalData.Session.CurrentCategory = category;
			}
			else if (treeViewItem.Header is AllSongs)
			{
				GlobalData.Session.ShowAllSongs();
			}
			else return;

            // Do a refresh
            RestoreFilteringAndSorting(GlobalData.Session.CurrentFilter, GlobalData.Session.CurrentSortDirective);
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();	

			// If something was done, do the regular stuff a left click does
			treeViewItem.IsSelected = true;
			treeViewItem.Focus();
			e.Handled = true;
		}

		private void CategoryViewRightMouseDown(object sender, MouseButtonEventArgs e)
		{
			TreeViewItem ClickedTreeViewItem = new TreeViewItem();

			// Find the original object that raised the event
			UIElement ClickedItem = VisualTreeHelper.GetParent(e.OriginalSource as UIElement) as UIElement;

			// Find the clicked TreeViewItem
			while ((ClickedItem != null) && !(ClickedItem is TreeViewItem))
			{
				ClickedItem = VisualTreeHelper.GetParent(ClickedItem) as UIElement;
			}

			ClickedTreeViewItem = ClickedItem as TreeViewItem;
			ClickedTreeViewItem.IsSelected = true;
			ClickedTreeViewItem.Focus();
		}

		#endregion

        #region Thumbnail Button Events

        public void Thumnail_Play(object sender, EventArgs e)
        {
            MusicPlayer.Play();
        }

        public void Thumnail_Pause(object sender, EventArgs e)
        {
            MusicPlayer.Pause();
        }

        private void Thumnail_Previous(object sender, EventArgs e)
        {
            MusicPlayer.PlayPreviousSong();
        }

        private void Thumnail_Next(object sender, EventArgs e)
        {
            MusicPlayer.PlayNextSong();
        }

        #endregion
    }

	#region Tree View Data Classes

	public class CategoryGroup
	{
		public CategoryGroup()
		{
			this.Items = GlobalData.Library.Categories;
			this.Name = "Categories";
		}

		public String Name { get; set; }

		public ObservableCollection<Category> Items { get; set; }
	}

	public class AllSongs
	{
		public AllSongs()
		{
			Name = "All Songs";
		}

		public String Name { get; set; }
	}

	#endregion
}
