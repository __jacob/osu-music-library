﻿using OsuMusicLibrary.Data;
using OsuMusicLibrary.Data.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WinForms = System.Windows.Forms;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for Settings.xaml
	/// </summary>
	public partial class SettingsWindow : Window
	{
		public SettingsWindow()
		{
			InitializeComponent();
            Settings settings = GlobalData.Settings;

            // Textbox
            FilePathBox.Text = GlobalData.Settings.OsuInstallPath;

            // Combobox
            ComboBoxNextSong.SelectedItem = settings.SwitchSongs ? PlayNew : (settings.AutoRepeat ? AutoRepeat : Stop);

            // Checkbox
            IgnorePreviewTime.IsChecked = GlobalData.Settings.IgnorePreviewTime;
			Unicode.IsChecked = GlobalData.Settings.PreferUnicode;
            Shuffle.IsChecked = GlobalData.Settings.Shuffle;
			AutoSynch.IsChecked = GlobalData.Settings.AutoSynch;
            ConfirmMulti.IsChecked = GlobalData.Settings.ConfirmMultiActions;
		}

		private void OpenButton_Click(object sender, RoutedEventArgs e)
		{
			var dialog = new WinForms.FolderBrowserDialog();
			var result = dialog.ShowDialog();

			if (result == WinForms.DialogResult.OK)
			{
				String selectedPath = dialog.SelectedPath;

				// Attempt to update the install path
				if (GlobalData.Settings.TryUpdateOsuInstallPath(selectedPath))
				{
					// Update the textbox and save
					FilePathBox.Text = selectedPath;
					GlobalData.SaveSettings();
					GlobalData.CreateNewSongCache();
					GlobalData.SongCache.Synchronize();
					DialogBox.Display("Path updated", "Successfully updated the osu! installation path.");
				}
				else
				{
					// Path was invalid. Show a popup warning and keep the previous window open.
					DialogBox.Display("Invalid file path", "That path is not a valid osu! directory. Make sure that you didn't select any of the sub-directories.");
				}
			}
		}

        /**
		 * Update the song selection based on the current selection
		 */
        private void ComboBoxNextSong_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ComboBoxNextSong.SelectedItem == Stop) { GlobalData.Settings.SwitchSongs = false; GlobalData.Settings.AutoRepeat = false; }
            if (ComboBoxNextSong.SelectedItem == AutoRepeat) { GlobalData.Settings.SwitchSongs = false; GlobalData.Settings.AutoRepeat = true; }
            if (ComboBoxNextSong.SelectedItem == PlayNew) { GlobalData.Settings.SwitchSongs = true; }
        }

		private void IgnorePreview_Checked(object sender, RoutedEventArgs e)
		{
			GlobalData.Settings.IgnorePreviewTime = true;
			GlobalData.SaveSettings();
		}

		private void IgnorePreview_Unchecked(object sender, RoutedEventArgs e)
		{
			GlobalData.Settings.IgnorePreviewTime = false;
			GlobalData.SaveSettings();
		}

		private void Unicode_Checked(object sender, RoutedEventArgs e)
		{
			GlobalData.Settings.PreferUnicode = true;
			GlobalData.SaveSettings();
			CollectionViewSource.GetDefaultView(MainWindow.OMLWindow.SongList.ItemsSource).Refresh();
		}

		private void Unicode_Unchecked(object sender, RoutedEventArgs e)
		{
			GlobalData.Settings.PreferUnicode = false;
			GlobalData.SaveSettings();
			CollectionViewSource.GetDefaultView(MainWindow.OMLWindow.SongList.ItemsSource).Refresh();
		}

		private void AutoSynch_Checked(object sender, RoutedEventArgs e)
		{
			GlobalData.Settings.AutoSynch = true;
			GlobalData.SaveSettings();
			SongScanner.Start();
		}

		private void AutoSynch_Unchecked(object sender, RoutedEventArgs e)
		{
			GlobalData.Settings.AutoSynch = false;
			GlobalData.SaveSettings();
			SongScanner.Stop();
		}

        private void Shuffle_Changed(object sender, RoutedEventArgs e)
		{
            if (Shuffle.IsChecked != null)
            {
                GlobalData.Settings.Shuffle = (bool)Shuffle.IsChecked;
                GlobalData.SaveSettings();
            }
		}

        private void ConfirmMultipleActions_Changed(object sender, RoutedEventArgs e)
        {
            if (ConfirmMulti.IsChecked != null)
            {
                GlobalData.Settings.ConfirmMultiActions = (bool)ConfirmMulti.IsChecked;
                GlobalData.SaveSettings();
            }
        }
	}
}
