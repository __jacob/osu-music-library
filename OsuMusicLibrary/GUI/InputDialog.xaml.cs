﻿using OsuMusicLibrary.Data.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for InputDialog.xaml
	/// </summary>
	public partial class InputDialog : Window
	{
		public delegate bool Verify(String text);
		public delegate void Complete(String text);

		private Verify verifyCall;
		private Complete completeCall;

		public InputDialog(String title, String description, String defaultValue, String buttonText, Verify verifyCall, Complete completeCall)
		{
			this.verifyCall = verifyCall;
			this.completeCall = completeCall;
			InitializeComponent();
			Title = title;
			Description.Content = description;
			TextField.Text = defaultValue;
			ButtonText.Content = buttonText;
		}

		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			if (verifyCall(TextField.Text))
			{
				completeCall(TextField.Text);
				this.Close();
			}
		}

		public static void Display(String title, String description, String defaultValue, String buttonText, Verify verifyCall, Complete completeCall)
		{
			InputDialog window = new InputDialog(title, description, defaultValue, buttonText, verifyCall, completeCall);
			window.Show();
		}
	}
}
