﻿using OsuMusicLibrary.Data.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for SongInfo.xaml
	/// </summary>
	public partial class SongInfo : Window
	{
		public SongInfo(SongData song)
		{
			InitializeComponent();
			BeatmapList.ItemsSource = song.Beatmaps;
			SongTitle.Content = song.DisplayTitle;
			Artist.Content = song.DisplayArtist;
			Source.Content = song.DisplaySource;
            Time.Content = song.DisplayTime;
			Creator.Content = song.DisplayCreator;
            ID.Content = song.SongID;
			Categories.Content = song.DisplayCategories;
			Tags.Text = song.DisplayTags;
		}
	}
}
