﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for ConfirmationDialog.xaml
	/// </summary>
	public partial class ConfirmationDialog : Window
	{
		public delegate void NextCall();

		String message;
		private NextCall yesCall;
		private NextCall noCall;

		public ConfirmationDialog(String title, String message, NextCall yesCall, NextCall noCall, int height)
		{
			this.message = message;
			this.yesCall = yesCall;
			this.noCall = noCall;
			InitializeComponent();
			this.Title = title;
			this.Height = height;
		}

		public static void Display(String title, String message, NextCall yes, NextCall no)
		{
			ConfirmationDialog confirmation = new ConfirmationDialog(title, message, yes, no, 180);
			confirmation.Show();
		}

		public static void Display(String title, String message, NextCall yes, NextCall no, int height)
		{
			ConfirmationDialog confirmation = new ConfirmationDialog(title, message, yes, no, height);
			confirmation.Show();
		}

		private void Yes_Click(object sender, RoutedEventArgs e)
		{
			yesCall();
			this.Close();
		}

		private void No_Click(object sender, RoutedEventArgs e)
		{
			noCall();
			this.Close();
		}

		private void Textblock_Loaded(object sender, RoutedEventArgs e)
		{
			TextBlock textblock = sender as TextBlock;
			textblock.Text = message;
		}
	}
}
