﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for dialogBox.xaml
	/// </summary>
	public partial class DialogBox : Window
	{
		public delegate void NextCall();
		private NextCall next;
		private String message;

		public DialogBox(String title, String message, NextCall next, int height)
		{
			this.next = next;
			this.message = message;
			InitializeComponent();
			if (title != null) Title = title;
			this.Height = height;
		}

		private void Textblock_Loaded(object sender, RoutedEventArgs e)
		{
			var textblock = sender as TextBlock;
			textblock.Text = message;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			next();
			this.Close();
		}

		public static DialogBox Display(String title, String message, NextCall next, int height)
		{
			DialogBox box = new DialogBox(title, message, next, height);
			box.Height = height;
			box.Show();
			return box;
		}

		public static DialogBox Display(String title, String message, NextCall next)
		{
			return Display(title, message, next, 120);
		}

		public static DialogBox Display(String title, String message)
		{
			return Display(title, message, () => { /* no-op */ });
		}

		public static DialogBox DisplayError(String title, String message, NextCall next, int height)
		{
			App.SetShutdownMode(ShutdownMode.OnLastWindowClose);
			return Display("Error: " + title, message, next, height);
		}

		public static DialogBox DisplayError(String title, String message, NextCall next)
		{
			return Display("Error: " + title, message, next, 360);
		}
	}
}
