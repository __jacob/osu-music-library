﻿using OsuMusicLibrary;
using OsuMusicLibrary.Data;
using OsuMusicLibrary.Data.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WinForms = System.Windows.Forms;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for InstallationPathdialog.xaml
	/// </summary>
	public partial class InstallationPathDialog : Window
	{
		public delegate void NextCall();

		private NextCall next;
		private String filepath;

		public InstallationPathDialog(String filepath, String message, NextCall next)
		{
			this.next = next;
			this.filepath = filepath;
			InitializeComponent();
			FilePathBox.Text = filepath;
			Message.Text = message;
		}

		public static void Display(String message, String filepath, NextCall next)
		{
			InstallationPathDialog installPathdialog = new InstallationPathDialog(filepath, message, next);
			installPathdialog.Show();
		}

		private void OpenButton_Click(object sender, RoutedEventArgs e)
		{
			var dialog = new WinForms.FolderBrowserDialog();
			var result = dialog.ShowDialog();

			if (result == WinForms.DialogResult.OK)
			{
				// Updates the current file path. Note that no verification is done at this point
				String selectedPath = dialog.SelectedPath;
				filepath = selectedPath;
				FilePathBox.Text = selectedPath;
			}
		}

		private void DoneButton_Click(object sender, RoutedEventArgs e)
		{
			// Attempt to update the install path
			if (GlobalData.Settings.TryUpdateOsuInstallPath(filepath))
			{
				// Successfully set a valid path
				next();
				this.Close();
			}
			else
			{
				// Path was invalid. Show a popup warning and keep the previous window open.
				DialogBox.Display("Invalid file path", "The current path is not a valid osu! directory. Make sure that you didn't select any of the sub-directories.");
			}
		}
	}
}
