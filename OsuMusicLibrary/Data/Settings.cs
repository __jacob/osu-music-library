﻿using System;
using System.IO;
using Newtonsoft.Json;
using OsuMusicLibrary.Data.Containers;
using OsuMusicLibrary.Data.Types;
using OsuMusicLibrary.Data.Util;

namespace OsuMusicLibrary.Data
{
	[JsonObject(MemberSerialization.OptIn)]
	public class Settings
	{
		/** The osu! installation path */
		[JsonProperty]
		public String OsuInstallPath { get; private set; }

		/** The current maximum number of stars allowed. */
		[JsonProperty]
		public int MaxRating { get; set; }

		/** Keep playing the song until the user pauses. */
		[JsonProperty]
		public bool AutoRepeat { get; set; }

		/** Move on to another song after completing the current song */
		[JsonProperty]
		public bool SwitchSongs { get; set; }

		/** Whether or not songs should be picked randomly */
        [JsonProperty]
        public bool Shuffle { get; set; }

		/** Play the song from the beginning instead of the preview time */
		[JsonProperty]
		public bool IgnorePreviewTime { get; set; }

		/** Display the unicode versions of titles and artists by default */
		[JsonProperty]
		public bool PreferUnicode { get; set; }

		/** Whether or not the song scanner should run synchronizations in the background. */
		[JsonProperty]
		public bool AutoSynch { get; set; }

        /** Whether confirmation messages should be shown whenever multiple songs are being affected at once */
        [JsonProperty]
        public bool ConfirmMultiActions { get; set; }


		public Settings()
		{
			OsuInstallPath = null;
			MaxRating = 5;
			AutoRepeat = false;
			SwitchSongs = true;
			Shuffle = true;
			IgnorePreviewTime = false;
			PreferUnicode = false;
			AutoSynch = true;
            ConfirmMultiActions = true;
		}

		/** 
		 * Return the path to the song directory 
		 */
		//[JsonIgnore]
		public String SongsDirectory { get { return OsuInstallPath == null ? null : Path.Combine(OsuInstallPath, "Songs"); } }

		/**
		 * Attempt to update the installation path for osu!.
		 */
		public bool TryUpdateOsuInstallPath(String directory)
		{
			if (InstallLocator.IsValidInstallPath(directory))
			{
				OsuInstallPath = directory;
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
