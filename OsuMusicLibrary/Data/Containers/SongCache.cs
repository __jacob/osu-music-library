﻿using Newtonsoft.Json;
using OsuMusicLibrary.Data.Containers;
using OsuMusicLibrary.Data.Util;
using OsuMusicLibrary.GUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OsuMusicLibrary.Data.Containers
{
	/**
	 * The keeps a local cache of all the songs in the osu! 'Songs' dicrectory. This data is stored on disk using 
	 * Json. The cache regularly synchronizes its content with the osu! 'songs' folder to keep up to date.
	 */
	public class SongCache
	{
		/** The list of all song data */
		public readonly SortedList<int, SongData> Songs = new SortedList<int, SongData>();

		/**
		 * Synchronizes the content of the cache with the 'songs' directory in order to add any new songs and get rid 
		 * of old ones. Initially, this means retrieving information about every song available (locally) in osu!. However 
		 * once the songs have been cached, subsequent synchronizations can skip a majority of all songs.
		 */
		public void Synchronize()
		{
			// If the current installtion path is not valid, don't try to synchronize
			if (!InstallLocator.IsValidInstallPath(GlobalData.Settings.OsuInstallPath)) return;

			Console.WriteLine("Synchronizing Song Cache...");
			
			// Flag whether there are any new changes that need saving
			bool hasChanged = false;
			// Set of the IDs of all the current valid songs in the osu! directory
			HashSet<int> currentSongs = new HashSet<int>();

			// Look through the 'Songs' directory for new songs
			foreach (String directoryPath in Directory.GetDirectories(GlobalData.Settings.SongsDirectory))
			{
				try
				{
					String directoryName = Path.GetFileName(directoryPath);
					int songID = SongLoader.GetSongID(directoryName);
					
					// Only add new songs
					if (!Songs.ContainsKey(songID))
					{
						// Load the song
						SongData song = SongLoader.LoadSongData(directoryPath, directoryName);
						
						// If the song was successfully loaded, add it to the list
						Songs.Add(song.SongID, song);
						hasChanged = true;

						Console.WriteLine("Loaded song: " + song.Title + " - " + song.Artist + ", ID = " + song.SongID);
					}

					// If the song exists, 
					currentSongs.Add(songID);
				}
				catch (InvalidSongException/* ise - deal with non-InvalidSongErrors... */)
				{
					Console.WriteLine("Failed to load song: " + directoryPath);
				}
			}

			// Remove old songs that have been deleted from the 'Songs' directory
			foreach (KeyValuePair<int, SongData> keyvalue in Songs.Reverse())
			{
				if (!currentSongs.Contains(keyvalue.Key))
				{
					Songs.Remove(keyvalue.Key);
					hasChanged = true;

					Console.WriteLine("Removed song: " + keyvalue.Value.Title + " - " + keyvalue.Value.Artist + ", ID = " + keyvalue.Value.SongID);
				}
			}

			// If the song cache has changed, save the changes
			if (hasChanged)
			{
				GlobalData.SaveSongCache();
			}

			// Update the list used in the application
			updateObservableCollection();
		}

		private void updateObservableCollection()
		{
			// Remove deleted songs from the observable list
			foreach (SongData song in GlobalData.ObservableSongList.ToArray())
			{
				if (!Songs.ContainsKey(song.SongID))
				{
					GlobalData.ObservableSongList.Remove(song);
				}
			}

			// Add songs that aren't in the list already
			foreach (KeyValuePair<int, SongData> keyValue in Songs)
			{
				if (!GlobalData.ObservableSongList.Contains(keyValue.Value))
				{
					GlobalData.ObservableSongList.Add(keyValue.Value);
				}
			}

			try
			{
				// Update the interface
				CollectionViewSource.GetDefaultView(MainWindow.OMLWindow.SongList.ItemsSource).Refresh();
			}
			catch (NullReferenceException) { }
		}
	}
}
