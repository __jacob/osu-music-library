﻿using Newtonsoft.Json;
using OsuMusicLibrary.Data.Types;
using System;
using System.Collections.Generic;

namespace OsuMusicLibrary.Data.Containers
{
    [JsonObject(MemberSerialization.OptIn)]
	public class Category
	{
		public Category(String name)
		{
			Name = name;
            SongIDs = new HashSet<int>();
            Sort = new SortDirective();
            Filter = new Filter(this);
		}

		public void AddSong(SongData song)
		{
			if (song.IsValid() && !SongIDs.Contains(song.SongID))
			{
				SongIDs.Add(song.SongID);
			}
		}

        [JsonProperty]
		public String Name { get; set; }

        [JsonProperty]
        public HashSet<int> SongIDs;

        public Filter Filter;

        public SortDirective Sort;
	}
}
