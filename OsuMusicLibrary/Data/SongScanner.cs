﻿using OsuMusicLibrary.Data;
using OsuMusicLibrary.Data.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace OsuMusicLibrary.Data
{
	/**
	 * Synchronizes the library with the songs in osu!'s 'Songs' directory. The synchronization is done using
	 * a DispatcherTimer to ensure that it's performed on the same thread as the gui is handled in, thus avoiding
	 * the issue of thread safety by sticking to a single thread throughout the application.
	 */
	class SongScanner
	{
		private static bool enabled = false;
		private static readonly DispatcherTimer Timer = new DispatcherTimer();

		static SongScanner()
		{
			Timer.Tick += timerTick;
		}

		public static void SetInterval(TimeSpan time)
		{
			Timer.Interval = time;
		}

		public static void Start()
		{
			if (!enabled)
			{
				enabled = true;
				Timer.Start();
			}
		}

		public static void Stop()
		{
			enabled = false;
		}

		private static void timerTick(object sender, EventArgs e)
		{
			if (enabled)
			{
				GlobalData.SongCache.Synchronize();
			}
			else
			{
				Timer.Stop();
			}
		}
	}
}
