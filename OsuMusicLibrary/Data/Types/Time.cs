﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsuMusicLibrary.Data.Containers
{
	[JsonObject(MemberSerialization.OptIn)]
	public class Time
	{
		/** <summary> The total number of milliseconds. </summary> */
		[JsonProperty]
		public int TotalMilliSeconds { get; set; }

		public Time(int milliseconds)
		{
			TotalMilliSeconds = milliseconds;
		}

		public override string ToString()
		{
			return new TimeSpan(0,0,0,0,TotalMilliSeconds).ToString(@"mm\:ss");
		}
	}
}
