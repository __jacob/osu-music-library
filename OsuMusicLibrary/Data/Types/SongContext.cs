﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsuMusicLibrary.Data.Types
{
    public enum SongContext
    {
        ALL_SONGS,
        CATEGORY,
        QUEUE
    }
}
