﻿using Newtonsoft.Json;
using OsuMusicLibrary.Data.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsuMusicLibrary.Data.Containers
{
	public class BeatmapData
	{
		// Default constructor required by the json deserializer
		private BeatmapData(){}

		public BeatmapData(GameMode mode, String difficultyName, int overallDifficulty)
		{
			Mode = mode;
			VersionName = difficultyName;
			OverallDifficulty = overallDifficulty;
		}

		public BeatmapData(GameMode mode, String difficultyName) : this(mode, difficultyName, -1) { }

		public BeatmapData(GameMode mode) : this(mode, null) {}

		public GameMode Mode { get; set; }
		public String VersionName { get; set; }
		public int OverallDifficulty { get; set; }

		[JsonIgnore]
		public String GameModeName { get { return EnumHelper.GetEnumDescription(Mode); } }

		[JsonIgnore]
		public String CalculatedDifficulty { get { return EnumHelper.GetEnumDescription(DifficultyParser.GetDifficulty(OverallDifficulty)); } }
	}
}
