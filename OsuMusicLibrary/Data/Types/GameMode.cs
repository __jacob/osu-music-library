﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsuMusicLibrary.Data.Containers
{
	public enum GameMode 
	{
		[Description("osu!")]
		OSU = 0,
		[Description("Taiko")]
		TAIKO = 1,
		[Description("Catch The Beat")]
		CATCH_THE_BEAT = 2,
		[Description("osu!mania")]
		OSU_MANIA = 3
	}
}
