﻿using OsuMusicLibrary.Data.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsuMusicLibrary.Data.Types
{
    public class SortDirective
    {
        public String PropertyName { get; set; }
        public Func<SongData, object> SortProperty { get; set; }
        public bool IsAscending { get; set; }

        public SortDirective()
        {
            PropertyName = "SongID";
            SortProperty = (song => song.SongID);
            IsAscending = true;
        }
    }
}
