﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsuMusicLibrary.Data.Util
{
	public class TimeFormat
	{
		private DateTime then;

		public TimeFormat(DateTime then)
		{
			this.then = then;
		}

		public override string ToString()
		{
			return TimeSince(DateTime.Now, then);
		}

		public static String TimeSince(DateTime now, DateTime then)
		{
			if (now.Year == then.Year)
			{
				if (now.Month == then.Month)
				{
					if (now.Day == then.Day)
					{
						if (now.Hour == then.Hour)
						{
							if (now.Minute >= then.Minute)
							{
								return MinutesAgo(now.Minute - then.Minute);
							}
						}
						else if (now.Hour > then.Hour)
						{
							return HoursAgo(now.Hour - then.Hour, now.Minute - then.Minute);
						}
					}
					else if (now.Day > then.Day)
					{
						return DaysAgo(now.Day - then.Day, now.Hour - then.Hour, now.Minute - then.Minute);
					}
				}
				else if (now.Month > then.Month)
				{
					return MonthsAgo(now.Month - then.Month, now.Day - then.Day, now.Hour - then.Hour, now.Minute - then.Minute, then);
				}
			}
			else if (now.Year > then.Year)
			{
				return YearsAgo(now.Year - then.Year, now.Month - then.Month, now.Day - then.Day, now.Hour - then.Hour, now.Minute - then.Minute, then);
			}
			return "Time traveler! :D";
		}

		public static String MinutesAgo(int minutes)
		{
			return MinutesToString(minutes);
		}

		public static String HoursAgo(int hours, int minutes)
		{
			if(minutes < 0)
			{
				minutes += 60;
				hours--;
			}

			if (hours == 0)
			{
				return MinutesAgo(minutes);
			}
			else
			{
				return HoursToString(hours);
			}
		}

		public static String DaysAgo(int days, int hours, int minutes)
		{
			if (minutes < 0)
			{
				minutes += 60;
				hours--;
			}
			if (hours < 0)
			{
				hours += 24;
				days--;
			}

			if (days == 0)
			{
				return HoursAgo(hours, minutes);
			}
			else
			{
				return DaysToString(days);
			}
		}

		public static String MonthsAgo(int months, int days, int hours, int minutes, DateTime then)
		{
			if (minutes < 0)
			{
				minutes += 60;
				hours--;
			}
			if (hours < 0)
			{
				hours += 24;
				days--;
			}
			if (days < 0)
			{
				days += DateTime.DaysInMonth(then.Year, then.Month);
				months--;
			}

			if (months == 0)
			{
				return DaysAgo(days, hours, minutes);
			}
			else
			{
				return MonthsToString(months);
			}
		}

		public static String YearsAgo(int years, int months, int days, int hours, int minutes, DateTime then)
		{
			if (minutes < 0)
			{
				minutes += 60;
				hours--;
			}
			if (hours < 0)
			{
				hours += 24;
				days--;
			}
			if (days < 0)
			{
				days += DateTime.DaysInMonth(then.Year, then.Month); ;
				months--;
			}
			if (months < 0)
			{
				months += 12;
				years--;
			}

			if (years == 0)
			{
				return MonthsAgo(months, days, hours, minutes, then);
			}
			else
			{
				return YearsToString(years);
			}
		}

		public static String MinutesToString(int minutes)
		{
			if (minutes == 0)
			{
				return "A moment ago!";
			}
			else if (minutes == 1)
			{
				return minutes + " minutes ago";
			}
			else
			{
				return minutes + " minutes ago";
			}
		}

		public static String HoursToString(int hours)
		{
			if (hours == 1)
			{
				return hours + " hour ago";
			}
			else
			{
				return hours + " hours ago";
			}
		}

		public static String DaysToString(int days)
		{
			if (days == 1)
			{
				return days + " day ago";
			}
			else
			{
				return days + " days ago";
			}
		}

		public static String MonthsToString(int months)
		{
			if (months == 1)
			{
				return months + " month ago";
			}
			else
			{
				return months + " months ago";
			}
		}

		public static String YearsToString(int years)
		{
			if (years == 1)
			{
				return years + " year ago";
			}
			else
			{
				return years + " years ago";
			}
		}
	}
}
