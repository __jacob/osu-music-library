﻿using OsuMusicLibrary.Data.Containers;
using OsuMusicLibrary.Data.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsuMusicLibrary.Data.Util
{
    public class Session
    {
        public Session()
        {
            CurrentCategory = null;
            AllSongsFilter = new Filter(null);
            AllSongsSort = new SortDirective();
        }

        /** Temporary queue of songs */
        public readonly List<SongData> Queue = new List<SongData>();

        /** The currently selected song list */
        public Category CurrentCategory { get; set; }

        /** The filter belonging to the current category (or all songs list if no category is selected) */
        public Filter CurrentFilter { get { return CurrentCategory != null ? CurrentCategory.Filter : AllSongsFilter; } }

        /** The sort directive belonging to the current category (or all songs list if no category is selected) */
        public SortDirective CurrentSortDirective { get { return CurrentCategory != null ? CurrentCategory.Sort : AllSongsSort; } }

        /** Whether categories should be ignored */
        public bool AllSongsVisible { get { return CurrentCategory == null; } }


        /** The filter for the 'All Songs' list */
        public Filter AllSongsFilter { get; private set; }

        /** Getter for the library */
        public SortDirective AllSongsSort { get; private set; }


        public void ShowAllSongs()
        {
            CurrentCategory = null;
        }
    }
}
