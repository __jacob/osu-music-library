﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsuMusicLibrary.Data.Util
{
	class InstallLocator
	{
		/**
		 * Attempts to find the osu! installation directory in standard locations. Either teturns a valid 
		 * installation path, or null if none could be detected.
		 */
		public static String TryFindInstallPath()
		{
			// Get an array of paths to test
			String[] paths = getExpectedInstallPaths();

			// For each of the paths, see if it exists. If it does, return it.
			foreach(String path in paths)
			{
				if(IsValidInstallPath(path))
				{
					return path;
				}
			}

			// If no installation path was detected, return null
			return null;
		}

		/**
		 * Determines whether a directory is a valid osu! installation directory.
		 */
		public static bool IsValidInstallPath(String path)
		{
			// If the installation path exists and it contains a "songs" directory, it's considered valid.
			return Directory.Exists(path) && Directory.Exists(Path.Combine(path, "songs"));
		}

		/**
		 * Returns typical installation paths for osu!
		 */
		private static String[] getExpectedInstallPaths()
		{
			return new String[] {"C:/Program Files (x86)/osu!/", "C:/Program Files/osu!/"};
		}
	}
}
