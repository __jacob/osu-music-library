﻿using System;
using System.IO;
using System.Security;
using System.Windows;
using OsuMusicLibrary.Data;
using OsuMusicLibrary.GUI;
using OsuMusicLibrary.Program;
using OsuMusicLibrary.Util;
using OsuMusicLibrary.Media;

namespace OsuMusicLibrary
{
	/// <summary> StartupUri="GUI/MainWindow.xaml"
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private static Application app;
		public static bool Initialized = false;

        // Keyboard listener used to listen for media keys
        KeyboardListener KListener = new KeyboardListener();

		void App_Startup(object sender, StartupEventArgs e)
		{
			app = this;
#if DEBUG
			// Enable the console
			ConsoleManager.Show();
#endif
            // Add a global hook to listen for keyboard presses
            KListener.KeyUp += new RawKeyEventHandler(KListener_KeyUp);

			// Make sure the program has write access
			if (!HasWriteAccessToFolder("/"))
			{
				DialogBox.Display("Access denied", "The program doesn't have write access to the current directory. Either move the program or give it administrator rights.");
				return;
			}

            // Add an exception handler in case any exceptions slip through 
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CrashHandler);

			Initializer.Run();
		}

		private void App_Exit(object sender, ExitEventArgs e)
		{
            // Remove the global hook
            KListener.Dispose();

			if (Initialized)
			{
				GlobalData.SaveSettings();
				GlobalData.SaveLibrary();
				Console.WriteLine("!");
			}
		}

        static void CrashHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            Logger.LogException(e);
        }

        public static bool HasWriteAccessToFolder(string folderPath)
        {
            try
            {
                // Attempt to save a file to see if we have write access
                const string filepath = "___WRITE_ACCESS_TEST___.txt";
                File.WriteAllText(filepath, "File used to test write access. It can safely be deleted.");
                File.Delete(filepath);
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
            catch (IOException)
            {
                return false;
            }
            catch (NotSupportedException)
            {
                return false;
            }
            catch (SecurityException)
            {
                return false;
            }
        }

        void KListener_KeyUp(object sender, RawKeyEventArgs args)
        {
            if (OsuMusicLibrary.GUI.MainWindow.OMLWindow.IsInitialized)
            {
                if (args.Key == System.Windows.Input.Key.MediaPlayPause)
                {
                    Invoke(() => {
                        if (MusicPlayer.Playing)
                        {
                             MusicPlayer.Pause();
                        }
                        else
                        {
                            MusicPlayer.Play();
                        }
                    });
                }
                if (args.Key == System.Windows.Input.Key.MediaNextTrack)
                {
                    Invoke(() => { MusicPlayer.PlayNextSong(); });
                }
                if (args.Key == System.Windows.Input.Key.MediaPreviousTrack)
                {
                    Invoke(() => { MusicPlayer.PlayPreviousSong(); });
                }
                if (args.Key == System.Windows.Input.Key.MediaStop)
                {
                    Invoke(() => { MusicPlayer.Stop(); });
                }
            }
            else
            {
                Console.WriteLine("NotInitialized");
            }
        }

        private static void Invoke(Action action)
        {
            OsuMusicLibrary.GUI.MainWindow.OMLWindow.Dispatcher.BeginInvoke(action);
        }

		public static void SetShutdownMode(ShutdownMode shutdownMode)
		{
			app.ShutdownMode = shutdownMode;
		}

		public static void Close()
		{
			app.Shutdown();
		}
	}
}
