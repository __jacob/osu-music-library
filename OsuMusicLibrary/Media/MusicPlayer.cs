﻿using OsuMusicLibrary.Data.Containers;
using OsuMusicLibrary.GUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using OsuMusicLibrary.Data;
using OsuMusicLibrary.Data.Types;

namespace OsuMusicLibrary.Media
{
	class MusicPlayer
	{
		public static MediaPlayer Player { get; private set; }
		private static DispatcherTimer timer;

        public static bool Playing { get; private set; }
        private static TimeSpan startPosition;
	
		static MusicPlayer()
		{
            Playing = false;

			Player = new MediaPlayer();
			Player.MediaEnded += onAudioStopped;
			Player.MediaFailed += onAudioStopped;
            Player.MediaOpened += onAudioOpened;

			timer = new DispatcherTimer();
			timer.Interval = TimeSpan.FromSeconds(1);
			timer.Tick += timerTick;
			timer.Start();
		}

		public static void SetCurrentSong(SongContext context, SongData song)
		{
			SongPicker.SetContext(context, song);
			loadSong(song);
		}

		private static void loadSong(SongData song)
		{
			if (!File.Exists(song.AudioFilePath)) return;

			// Load a new song
			Player.Open(new Uri(song.AudioFilePath));

			// Update the current position in the song to either the preview position or the beginning, depending on the settings
			startPosition = new TimeSpan(0, 0, 0, 0, GlobalData.Settings.IgnorePreviewTime ? 0 : song.PreviewTime.TotalMilliSeconds);

            // Try to set the current time. If the song isn't loaded, this will fail, but will then set the time from an event instead
            Player.Position = startPosition;

			// Update the descriptions in the GUI
			MainWindow.OMLWindow.Description1.Content = song.DisplayTitle + " - " + song.DisplayArtist;
			MainWindow.OMLWindow.Description2.Content = song.DisplaySource != "" ? song.DisplaySource : "Mapped by " + song.DisplayCreator;
			MainWindow.OMLWindow.Description3.Content = song.DisplaySource != "" ? "Mapped by " + song.DisplayCreator : "";
		}

		public static void Play()
		{
			if (Player.Source != null)
			{
				Player.Play();

                if (SongPicker.CurrentSong != null)
                {
                    MainWindow.OMLWindow.Title = SongPicker.CurrentSong.DisplayTitle + " by " + SongPicker.CurrentSong.DisplayArtist + " - Osu Music Library";
                }

                if (!Playing)
                {
                    Playing = true;

                    // Change the image of the buttons inside the application
                    MainWindow.OMLWindow.PlayPause.Content = MainWindow.OMLWindow.FindResource("Pause");

                    // Change the thumbnail buttons
                    MainWindow.OMLWindow.ThumbnailPlayPause.ImageSource = (ImageSource)MainWindow.OMLWindow.FindResource("BitmapPause");
                    MainWindow.OMLWindow.ThumbnailPlayPause.Description = "Pause";
                    MainWindow.OMLWindow.ThumbnailPlayPause.Click -= MainWindow.OMLWindow.Thumnail_Play;
                    MainWindow.OMLWindow.ThumbnailPlayPause.Click += MainWindow.OMLWindow.Thumnail_Pause;
                }
			}
		}

		public static void Pause()
		{
            if (Player.Source != null)
			{
				Player.Pause();
			}

            MainWindow.OMLWindow.Title = "Osu Music Library";

            if (Playing)
            {
                Playing = false;

                // Change the image of the buttons inside the application
                MainWindow.OMLWindow.PlayPause.Content = MainWindow.OMLWindow.FindResource("Play");

                // Change the thumbnail buttons
                MainWindow.OMLWindow.ThumbnailPlayPause.ImageSource = (ImageSource)MainWindow.OMLWindow.FindResource("BitmapPlay");
                MainWindow.OMLWindow.ThumbnailPlayPause.Description = "Play";
                MainWindow.OMLWindow.ThumbnailPlayPause.Click -= MainWindow.OMLWindow.Thumnail_Pause;
                MainWindow.OMLWindow.ThumbnailPlayPause.Click += MainWindow.OMLWindow.Thumnail_Play;
            }
		}

        public static void Stop()
        {
            Pause();
            Player.Stop();
        }

		public static void PlayNextSong()
		{
			SongData song = SongPicker.NextSong();
			if (song != null)
			{
				loadSong(song);
				Play();
			}
			else
			{
				Pause();
			}
		}

		public static void PlayPreviousSong()
		{
			SongData song = SongPicker.PreviousSong();
			if (song != null)
			{
				loadSong(song);
				Play();
			}
			else
            {
                Stop();
			}
		}

		/**
		 * Set the position in the song measured as a fraction of the whole song.
		 */
		public static void SetTime(double fraction)
		{
			if (Player.Source != null && Player.NaturalDuration.HasTimeSpan)
			{
				Player.Position = new TimeSpan(0, 0, 0, 0, (int)(Player.NaturalDuration.TimeSpan.TotalMilliseconds * fraction));
				Play();
			}
		}

        /**
		 * Set the player to the correct position in the song when it's completely loaded
		 */
        private static void onAudioOpened(object sender, EventArgs e)
        {
            Player.Position = startPosition;
        }

		/**
		 * Display the play button if the audio stopped
		 */
		private static void onAudioStopped(object sender, EventArgs e)
		{
			if (GlobalData.Settings.SwitchSongs)
			{
				PlayNextSong();
			}
			else
			{
				if (GlobalData.Settings.AutoRepeat)
				{
					Player.Position = new TimeSpan();
				}
				else
				{
					Pause();
				}
			}
		}

		/**
		 * Updates the text for the song time once a second.
		 */
		private static void timerTick(object sender, EventArgs e)
		{
			MainWindow window = MainWindow.OMLWindow;
			if (window != null)
			{
				if (Player.Source != null && Player.NaturalDuration.HasTimeSpan)
				{
                    try
                    {
                        window.CurrentPlayTime.Content = Player.Position.ToString(@"mm\:ss");
                        window.AudioLength.Content = Player.NaturalDuration.TimeSpan.ToString(@"mm\:ss");
                        window.SetSliderPosition(Player.Position.TotalMilliseconds / Player.NaturalDuration.TimeSpan.TotalMilliseconds);
                    }
                    catch (InvalidOperationException) 
                    {
                        Console.WriteLine("Why is this happening?!");
                    }
				}
				else
				{
					window.CurrentPlayTime.Content = "--:--";
					window.AudioLength.Content = "--:--";
				}
			}
		}
	}
}
